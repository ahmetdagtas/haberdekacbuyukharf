Vektörel Bilişim Java Programlama kursu ilk ödevi.

Bu projeden istenen, verilen metin içerisinde kaç adet büyük harf olduğunun saydırılması idi.

Proje bittiğinde .txt uzantılı bir dosya seçilerek içerisindeki toplam karakter sayısı, büyük harf sayısı, küçük harf sayısı, özel karakter sayısı, satır sayısı gibi değerlerin hesaplatılması hedefleniyor.