
public class BuyukHarf {
		public static void main(String[] args) {
			String deneme1 = "Ba�c�lar'da i�inde yolcular�n bulundu�u ��pheli bir otob�s, �stanbul polisini alarma ge�irdi. Durdurulan otob�s, detayl� bir aramadan ge�irildi. Kitap ve bro��rler bulundu�u otob�steki yakla��k 20 ki�i ise serbest b�rak�ld�. ��PHEL� OTOB�S DURDURULDU Ba�c�lar Mahmutbey Caddesi �zerinde bir yolcu otob�s�nden ��phelen polis ekipleri, otob�s� durdurdu. Otob�s, �SK� �ube M�d�rl��� kar��s�nda bulunan eski bir akaryak�t istasyonuna �ekildi. G�R�� VE �IKI�LAR YASAKLANDI Otob�s� incelemeleri i�in G�venlik �ube ve Ter�rle M�cadele Ekipleri sevk edildi. �evik Kuvvet ekipleri de akaryak�t istasyonu �evresinde yo�un g�venlik �nlemi alarak olay yerine giri� ve ��k��lar� yasaklad�. K�TAP VE BRO��R BULUNDU Otob�ste yap�lan incelemelerde �ok say�da kitap ve bro��r bulundu. Yap�lan incelemelerin ard�ndan otob�s polis, eskortlu�unda b�lgeden uzakla�t�r�ld�. 20 K��� SERBEST BIRAKILDI Otob�s i�indeki yakla��k 20 ki�i ise incelemelerin tamamlanmas�n�n ard�ndan serbest b�rak�ld�. Olay yerinden uzakla�an ��pheli ki�iler sorular� yan�ts�z b�rakt�.";
			String deneme2 = "Ahmet";
			String deneme3 = "Ahmet DA�tA�";
			
			int buyukHarfSayisi1 = 0;
			int buyukHarfSayisi2 = 0;
			int buyukHarfSayisi3 = 0;
			
			for (int i = 0; i < deneme1.length(); i++) {			
				if (Character.isUpperCase(deneme1.codePointAt(i)) == true) {
					buyukHarfSayisi1++;
				}
				else continue;			
			}
			
			for (int i = 0; i < deneme2.length(); i++) {			
				if (Character.isUpperCase(deneme2.codePointAt(i)) == true) {
					buyukHarfSayisi2++;
				}
				else continue;			
			}
			
			for (int i = 0; i < deneme3.length(); i++) {			
				if (Character.isUpperCase(deneme3.codePointAt(i)) == true) {
					buyukHarfSayisi3++;
				}
				else continue;			
			}
			
			System.out.println("B�y�k harf say�lar�");
			System.out.println("Birinci: " + buyukHarfSayisi1);
			System.out.println("�kinci: " + buyukHarfSayisi2);
			System.out.println("���nc�: " + buyukHarfSayisi3);
		}
	}